<?php

/**
 * This loads the app dependencies into the src/framework components.
 * This bootstrap/app.php file exists so that the app can be easily
 * built into multiple environments - CLI, Web/Router & Testing.
 */

require __DIR__ . '/../vendor/autoload.php';

use PetStore\Framework\Foundation\Application;
use App\Console\Collection as CommandCollection;
use App\Event\Collection as EventCollection;

/**
 * Load the application and dependencies. This includes .env config, database
 * connection and initializing a service container to resolve the dependencies
 * within the application.
 */
$app = (new Application(__DIR__.'/..'))

/**
 * Register the event listeners to the application to allow for loose
 * coupling between services within the application.
 */
    ->registerEvents(new EventCollection)

/**
 * Register the commands within the application. These will be loaded
 * into a symfony CLI application. That CLI is fetchable through
 * the app container as seen in the next step.
 */
    ->registerCommands(new CommandCollection);

return $app;
