<?php

namespace PetStore\Framework\Support;

class Collection
{
    protected $items = [];

    public function __construct($items = [])
    {
        ! empty($items) && $this->setItems($items);
    }

    /**
     * Get the items in the collection.
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * Reverse the $items in the collection and return them.
     * @return array
     */
    public function reverse($preserveKeys = false): array
    {
        return $this->items = \array_reverse($this->items, $preserveKeys);
    }

    /**
     * Set the items in the collection.
     * @return void
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }
}
