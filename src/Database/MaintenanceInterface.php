<?php

namespace PetStore\Framework\Database;

interface MaintenanceInterface
{
    /**
     * Make maintenance changes to the database.
     * @return void
     */
    public function commit();

    /**
     * Rollback database maintenance changes made by commit().
     * @return void
     */
    public function rollback();

    /**
     * Return the name of the affected table.
     * @return string
     */
    public function table();
}
