<?php

namespace PetStore\Framework\Database;

use Illuminate\Database\Capsule\Manager as Capsule;

class ConnectionFactory
{
    public static function create(): void
    {
        /**
         * @see developer note:
         *
         * There was an emphasis on utilizing a singleton pattern to
         * ensure there was a single database connection for the application.
         *
         * Illuminate\Database handles this within the Capsule when using global
         * static commands in the link below by using a static $instance (just
         * like a singleton) and a static connection() method to receive the
         * static instance.
         *
         * Static properties & methods are used as well inside the base model
         * class.
         *
         * I realize this isn't creating a custom singleton class for a database
         * connection. But I did prioritize the idea of utilizing a single
         * connection as well as including a very strong ORM.
         *
         * Capsule:
         * https://github.com/illuminate/database/blob/3f72c0acfa2737d571de0d4240182ab72bd9c4f3/Capsule/Manager.php#L199
         *
         * Model:
         * https://github.com/illuminate/database/blob/3f72c0acfa2737d571de0d4240182ab72bd9c4f3/Eloquent/Model.php#L1239
         */
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => getenv('DB_DRIVER'),
            'host'      => getenv('DB_HOST'),
            'database'  => getenv('DB_NAME'),
            'username'  => getenv('DB_USER'),
            'password'  => getenv('DB_PASS'),
            // Hardcoding sane default values:
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        // Make this Capsule instance available globally via static methods.
        $capsule->setAsGlobal();
        // Setup eloquent ORM
        $capsule->bootEloquent();
    }
}
