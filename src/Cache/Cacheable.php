<?php

namespace PetStore\Framework\Cache;

trait Cacheable
{
    /**
     * Set a unique identifier for this object to prevent naming conflicts,
     * also could be used to clear portions of the cache.
     * @return string
     */
    abstract public function cachePrefix(): string;

    /**
     * Pass in any type of data that can be uniquely identified in combination
     * with the cachePrefix().
     * @return string
     */
    public function getCacheKey($data): string
    {
        $json = json_encode($data);
        return $this->cachePrefix() . md5($json);
    }
}
