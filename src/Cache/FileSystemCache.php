<?php

namespace PetStore\Framework\Cache;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

class FileSystemCache extends FilesystemCachePool
{

    public function __construct($cacheDir)
    {
        $filesystemAdapter = new Local($cacheDir);
        $filesystem = new Filesystem($filesystemAdapter);

        parent::__construct($filesystem);
    }
}
