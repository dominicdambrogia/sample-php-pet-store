<?php

namespace PetStore\Test\Support;

use PHPUnit\Framework\TestCase;
use PetStore\Framework\Cache\FileSystemCache;

class FileSystemCacheTest extends TestCase
{
    public function testHasSetGetDelete()
    {
        $cache = new FileSystemCache(__DIR__);
        $key = 'test_key';

        $this->assertFalse($cache->has($key));

        $cache->set($key, true);
        $this->assertTrue($cache->get($key));

        $cache->delete($key);
        $this->assertFalse($cache->has($key));
    }
}
