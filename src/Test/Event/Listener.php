<?php

namespace PetStore\Framework\Test\Event;

use PetStore\Framework\Event\AbstractListener;
use League\Event\EventInterface;

class Listener extends AbstractListener
{
    public function targetEvent(): string
    {
        return 'test_event';
    }

    public function execute(EventInterface $event, $eventData = null): EventInterface
    {
        $event->data = 'test';
        return $event;
    }
}
