<?php

namespace PetStore\Test\Support;

use PHPUnit\Framework\TestCase;
use League\Event\Emitter;
use PetStore\Framework\Test\Event\Listener;

class ListenerTest extends TestCase
{
    public function testOnlyListenToTargetEvent()
    {
        $emitter = new Emitter;
        $listener = new Listener;

        $emitter->addListener('*', $listener);

        $event = $emitter->emit('test_event');

        $this->assertEquals($event->data, 'test');

        $event = $emitter->emit('random_event');

        $this->assertNull($event->data ?? null);
    }
}
