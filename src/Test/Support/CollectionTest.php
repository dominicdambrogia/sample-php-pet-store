<?php

namespace PetStore\Test\Support;

use PHPUnit\Framework\TestCase;
use PetStore\Framework\Support\Collection;

class CollectionTest extends TestCase
{
    public function testGetItems()
    {
        $collection = new Collection([1,2,3]);

        $this->assertEquals([1,2,3], $collection->getItems());
    }

    public function testReverse()
    {
        $collection = new Collection([1,2,3]);

        $this->assertEquals([3,2,1], $collection->reverse());
    }

    public function testSetItems()
    {
        $collection = new Collection;

        $collection->setItems([4,5,6]);
        $this->assertEquals([4,5,6], $collection->getItems());

        $collection->setItems([1,2,3]);
        $this->assertEquals([1,2,3], $collection->getItems());
    }
}
