<?php

namespace PetStore\Framework\Event;

use League\Event\AbstractListener as BaseAbstractListener;
use League\Event\EventInterface;

abstract class AbstractListener extends BaseAbstractListener
{
    /**
     * Return a string to declare which event identifier to run on.
     * @return string
     */
    abstract public function targetEvent(): string;

    /**
     * The function to use for child classes. This will only run
     * when the $event->name == $this->getTargetEvent();
     * When passing data between events and returning data, recommomended
     * practice is to use $event->data (public property).
     * @param EventInterface $event
     * @param mixed $eventData
     * @return EventInterface
     */
    abstract public function execute(EventInterface $event, $eventData = null): EventInterface;

    /**
     * Since all listeners use lazy wildcard events handle the logic of only
     * actually listening if this is the target event.
     * @return EventInterface
     */
    public function handle(EventInterface $event, $eventData = null)
    {
        return $event->getName() == $this->targetEvent()
            ? $this->execute($event, $eventData)
            : $event;
    }
}
