<?php

namespace PetStore\Framework\Foundation;

use League\Container\Container;
use League\Container\ReflectionContainer;
use League\Event\Emitter;
use PetStore\Framework\Database\ConnectionFactory as DatabaseConnection;
use PetStore\Framework\Support\Collection;
use Symfony\Component\Console\Application as CLI;
use Psr\SimpleCache\CacheInterface;
use PetStore\Framework\Cache\FileSystemCache;

final class Application
{
    /**
     * Our service container that holds the services for our application,
     * allows for IoC and Dependency Injectin.
     * @type ContainerBuilder
     */
    private $container;

    /**
     * The root directory of our project.
     * @type string
     */
    private $rootDir;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
        $this->container = new Container;

        $this->build();
    }

    /**
     * This loads the applications .env file via dotenv, and tells the service
     * container to lazy load and autowire class dependencies on the fly.
     * @return void
     */
    private function build(): void
    {
        // Load configs from root .env
        \Dotenv\Dotenv::create($this->rootDir)->load();
        // Initiliaze our database connection
        DatabaseConnection::create();
        // Allow for a reflection container to autoload dependencies.
        $this->container->delegate((new ReflectionContainer)->cacheResolutions());
        // Set PSR-16 Cache Interface in application to light weight filesystem cache
        $this->container->add(
            CacheInterface::class,
            new FileSystemCache($this->rootDir . "/storage")
        );
    }

    /**
     * Register a collection of commands and add them to the CLI
     * interface (symfony)
     * @return self
     */
    public function registerCommands(Collection $collection): self
    {
        $cli = new CLI;

        foreach ($collection->getItems() as $class) {
            $cli->add($this->container->get($class));
        }

        $this->container->share(CLI::class, $cli);

        return $this;
    }

    /**
     * Register a collection of events, add them to an event emitter and then
     * register the event emitter as a *shared* instance to load across
     * the application.
     * @return self
     */
    public function registerEvents(Collection $collection): self
    {
        $emitter = new Emitter;

        foreach ($collection->getItems() as $class) {
            $emitter->addListener('*', $this->container->get($class));
        }

        $this->container->share(Emitter::class, $emitter);

        return $this;
    }

    /**
     * Getter method for Service container that will hold all of the
     * services within our application. Useful for fetching public services
     * within our Container.
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }
}
