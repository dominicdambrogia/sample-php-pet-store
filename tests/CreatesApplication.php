<?php


namespace Tests;

trait CreatesApplication
{
    /**
     * Load the application to run the tests with.
     * @return PetStore\Framework\Foundation\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
