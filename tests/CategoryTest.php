<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Service\Catalog\Category\CategoryList;
use Tests\CreatesApplication;

class CategoryTest extends TestCase
{
    use CreatesApplication;

    public function testPrepareCategories()
    {
        $app = $this->createApplication();
        $cats = $app->getContainer()->get(CategoryList::class)->prepare();

        $this->assertEquals(3, count($cats));
    }
}
