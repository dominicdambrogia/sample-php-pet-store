# Table of Contents
+ [Introduction](https://gitlab.com/domdambrogia/sample-php-pet-store#introduction)
+ [Usage](https://gitlab.com/domdambrogia/sample-php-pet-store#usage)
+ [Thought Process](https://gitlab.com/domdambrogia/sample-php-pet-store#thought-process)
+ [Schema](https://gitlab.com/domdambrogia/sample-php-pet-store#schema)
+ [What Could Be Better](https://gitlab.com/domdambrogia/sample-php-pet-store#what-could-be-better)

# Introduction

My name is Dominic Dambrogia. I'm a Software Engineer currently working at [Dolls Kill](https://dollskill.com)
in San Francisco, California. Dolls Kill is an E-Commerce brand with over 2 million followers and 500k online users.
Their online store has 300k products and has 3000+ concurrent users on any given day and 7500+ users on peak traffic days (black friday, major sales, etc.)
Among many others found on my resueme, I have contributed to optimizing the performance of their site to allow for back to back million-dollar sales days over the black friday/cyber monday weekend. This was not possible before I came here and we've made great progress on a _very_ old and outdated site.

I have been in the field for about 7 years now and for the last 3 years I've been working with a lot of E-Commerce (Magento 1 & 2 + WooCommerce).
I enjoy CI/CD/TDD, docker, AWS Cloud, infrastructure as code (IaC), design patterns, MySQL, Elastic Search, Caching (redis, etc.) software architecture, system architecture / distrubuted systems, optimization and modern JavaScript.

The technologies I work with on a day to day basis are very enjoyable for me, however I do not like my company's culture. The vibe you get from [the homepage of the Dolls Kill site](https://dollskill.com) is the same as the in-office vibe. Personally for me it is not a good culture fit, so I'm looking to bring my talents else where.

Your company offers me a very similar technology stack I am experienced with and enjoy to work on. I'm hoping your company has a better culture fit for me and that the other devs are people I mesh well with. I'm a very big team player and thrive in a good team atmosphere. I try very hard to have a low ego to help bring up others (code reviews, buddy-coding, etc.), but also be confident in myself to accomplish challenging tasks when they inevitably come up.

Here is a list of some other sites I've built semi-recently. I did various work on all of these sites including backend, frontend, ERP integrations, catalog management, database management, etc. etc. etc. I would be glad to elaborate on how I contributed to each project individually.

[Virgin Orbit](https://virginorbit.com)
[ZippyPaws](https://zippypaws.com)
[Cremo](https://cremocompany.com)
[Anajet](https://anajet.com)

In my personal time I love riding motorcycles as well as both playing and watching basketball. And while I'm terrible at it, I enjoy sitting around and wasting a sunny day fishing.

I hope this gives you a little insite about who I am, my experience and what I'm looking for.

# Usage
These instructions assume you are in the project root and have comoser installed globally.

#### Set your `.env` file:

    cp .env.example .env

You can use Sqlite or MySQL. The `.env.example` file is setup to get you up
and running asap with Sqlite by only changing the path to the repo. But you
can definitely use MySQL if that is your desired db driver.

##### Migrate the schema, seed sample data:

    bin/console database:migrate
    bin/console database:seed

Optionaly you could also run the composer shortcut:

    composer setup

You can revert migration and seeds with the `--rollback` option. You can also
reset the composer setup with the following command if you've messed up before
this point. Be mindful of what order you run them in. For example: if you try to
seed data on an empty schema, you will have a bad time.

    bin/console database:migrate --rollback
    bin/console database:seed --rollback
    composer setup:reset

___
### You now have a working dataset to view data.
___

#### List available categories:

    bin/console catalog:categories

#### List available attributes and their meta data (sortable, filterable, values, etc.):

    bin/console catalog:attributes

#### List available products:

    bin/console catalog:products

The `--category <category>`, `--filter=<filterable_attribute>=value` and `--sort=<sortable_attribute [--desc]` options are available to filter the products as needed.

The values for `<category>`, `<filterable_attribute>` and `<sortable_attribute>` can be found in the `catalog:categories` and `catalog:attribute` commands.

For example, if you wanted to show all products that were cat toys and sort them by the most expensive items first you would run:


    bin/console catalog:products --category=toy --filter=pet-type=cat --sort=price --desc


You should receive the following skus (along with some other product meta data):


    toy-cat-nip
    toy-cat-mouse


# Thought Process

My general throught when doing any challenge like this isn't to build the latest and greatest proof of concept. I know that this is only for examples so I'm attempting to demonstrate knowledge of architecting a solution. There are _so_ many things that could be improved but I'm building an example of what I know and not remaking Magento. My goal is to present my knowledge while also delivering this in a realistic time frame / scope of work.

My very first thought when building this was what components I wanted to utilize. Since this was a CLI task, The [Symfony Console](https://symfony.com/doc/current/components/console.html) component was a no brainer.

The next and most important and most fundamental decision was to achieve [Inversion of Control](https://en.wikipedia.org/wiki/Inversion_of_control) for my application by utilizing a service container that would allow for auto Dependency Injecting my class dependencies. This is apparent in every major framework you use. I used [a very lightweight service container by The League](https://container.thephpleague.com/3.x/).

A good database tool will _always_ make your life easier, I decided to use Laravel's [Illuminate\Database](https://github.com/illuminate/database) package  (Eloquent ORM). I was deciding between Doctorine and Eloquent and I could have really chosen either since they are both decoupled very well. I know a singleton for a single persistent database connection was desired and I didn't build out a singleton class - but if you read my dev notes I thought this through and explained my decision.

I wanted to make the code as "deployable" as possible, so I used dotenv to load my .env vars. If I had more than the ~5 database values to set I would have likely broken the config into it's own class to store the data and share it across the application but it seemed like an overkill since it was only used it one single file.

I was excited to have a use for event driven architecture through [The Leagues Event Component](https://event.thephpleague.com/2.0/). I felt that it was a great solution for decoupling the coupon/sale to the product factory as well as the coupon sale conditionally affecting the sort order and allowing to resort the products. It kept my code separated and would allow for much further extensions.

Since the main purpose of this was a Catalog, I wanted to make that it's own service and pull the rest of the "framework"'s functionality into the Catalog. It also makes the catalog more testable and prevents the logic from occuring in the `App\Console\Command` namespace. Organizing the entire project in a domain structure like this would be a good direction moving forward - Basically move the models to their specific domain and either move the migrations/seeds to the root or include them in their domain. This just seemed like an overkill for the small project however, as well as out of scope.

E-Commerce schema is always complicated. There are a ton of relationships everywhere and you deal with _a lot_ of data. It can be expensive to query it often. To help combat this, I used a PSR-16 filesystem cache. It's easy to use with no external configurations and allows for a quick proof of concept. I cached the loaded products as well as the product collections. This helps so that if a product collection isn't loaded, at least the majority of the products in it will be cached as it is created.

There is a lot more going on than this. I would be very happy to discuss further!

# Schema

I added categories in to this just because I feel that they're _so_ fundamental to e-commerce and especially magento. I used two many-to-many relationships. A product can have many categories and a category can have many products as well as a product can have many attributes as well as an attribute can belong to many products. Realistically the attributes would be much more complicated for than that but again, I was just trying to make a proof of concept with the right fundamental ideas and I think the simple example the many-to-many relationship between attributes and products accomplished that.


# What could be better

The attribute relation is very weak. If you wanted to change how sizes were organized you'd have to update the values for each product as you update each value. I would be happy to explain how to explain a solution for this.

The application code structure doesn't scale very well. I started it with an intention to structure it as an MVC project when it probably would have done better in a domain driven structure. I think that this is pretty standard for most applications/frameworks. However with such a small example it didn't cause any problems.

Config settings were pretty minimal, with a basic .env. Adding configs per subject - database, cache, etc. would be better but I felt with the project size this wasn't beneficial and so I opted for the more minimal .env setup. Combining a larger .env setup and importing those values would be a happy medium point (like laravel does) but I'm not a fan of that because it creates a bloated .env file after a while. Not to mention that you use a single one-dimensional .env file to populate multiple-dimensional arrays within numerous different configurations. That has always bugged me about laravel. I actually prefer Magento 2's `app/etc/env.php` setup.

Autoloading services - If I utilized a PSR-4 format to glob-register classes into the service container rather than using the `Collection` classes in `App\Console` & `App\Event` I think that would have been a more elegant solution, but both are equally valid.

Thank you for taking the time to read this, I look forward to discussing further!
