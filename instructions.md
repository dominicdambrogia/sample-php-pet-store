### Pet Store
Imagine that you run an pet shop. You sell three different type of animals – Dogs, Cats, and Reptiles. You have multiple pets of each type available (i.e Hounds or Mutts or , Calicos, Turtles...). You also sell Pet toys and Pet Carriers.

Your store has a cataloging issue however! You need a way to search and filter your merchandise for easier visibility into your stores situation.

1. Your first challenge is to create and list all objects in the store.
2. Your second challenge is to sort and filter these objects by attributes associated to the objects.

The items in the store must share at least 1 attribute between them. If your catalog is sorted by an attribute, an object with this attribute not equal to the requested value will not show up.

**Attribute Ideas:**
- Pet Type
- Item Type
- Color
- Lifespan (Required)
- Age (Required)
- Price (Required)

Finally, your store offers discounts, but only on pets who's age is over half of it's project lifespan (i.e. Dogs live 12 years on average, so a dog at age 6 would be discounted).

Utilizing Object Oriented Programming, design and build a system that will store the listed information. Your submitted solution should contain not only the source code for the classes you have created, but also contain example usage of these classes. Please do not utilize pre-built frameworks.

**Your store must:**
- Have at least 5 of every object type (Dogs, Cats, Reptiles, Toys Carriers)
- Offer at least 5 items with a discount
- Sort and filter current objects by specific attributes of your choice (i.e. Item Type, Life Span, Pet Color)

You are not required to make a User Interface for this challenge. This can be run through CLI to demonstrate the listing, sorting and filtering abilities of the program. Should you desire, a UI will help demonstrate your competence, but will not set you apart positively if your underlying logic to sort and filter does not work

**Here are some answers to questions some have asked:**
- I assume that PHP is the preferred language for this OOP exercise? Yes, please use PHP. Please no frameworks.
- Should the storage of information be performed in a MySQL or other database, or do you mean that the information should just be stored in the objects I create for this project? MySql is preferred, but if you can manage to sort and filter without a database, go for it.
- Should the solution be completed with an HTML web form for ordering? It is not required, but can be used if you prefer. The solution does not need to be a web page, but demonstrating the relationship between PHP and HTML will only help your exercise.
- When submitting examples of usage, do you also mean screenshots or simply applications I write which utilize the objects I create? We would like to see the applications, not screenshots.
