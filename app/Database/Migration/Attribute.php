<?php

namespace App\Database\Migration;

use PetStore\Framework\Database\MaintenanceInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Model\Attribute as Model;

final class Attribute implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Capsule::schema()->create($this->table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('can_sort');
            $table->boolean('can_filter');
        });
    }

    public function rollback()
    {
        Capsule::schema()->drop($this->table());
    }
}
