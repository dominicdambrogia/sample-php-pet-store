<?php

namespace App\Database\Migration;

use PetStore\Framework\Database\MaintenanceInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Model\ProductCategory as Model;

final class ProductCategory implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Capsule::schema()->create($this->table(), function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->integer('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    public function rollback()
    {
        Capsule::schema()->drop($this->table());
    }
}
