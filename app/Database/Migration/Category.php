<?php

namespace App\Database\Migration;

use PetStore\Framework\Database\MaintenanceInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Model\Category as Model;

final class Category implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Capsule::schema()->create($this->table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
        });
    }

    public function rollback()
    {
        Capsule::schema()->drop($this->table());
    }
}
