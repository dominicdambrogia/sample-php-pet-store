<?php

namespace App\Database\Migration;

use PetStore\Framework\Database\MaintenanceInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Model\ProductAttribute as Model;

final class ProductAttribute implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Capsule::schema()->create($this->table(), function (Blueprint $table) {
            $table->increments('id');

            $table->integer('attribute_id');
            $table->foreign('attribute_id')->references('id')->on('attributes');

            $table->integer('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->text('value');
        });
    }

    public function rollback()
    {
        Capsule::schema()->drop($this->table());
    }
}
