<?php

namespace App\Database\Migration;

use PetStore\Framework\Database\MaintenanceInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Model\Product as Model;

final class Product implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Capsule::schema()->create($this->table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique();
        });
    }

    public function rollback()
    {
        Capsule::schema()->drop($this->table());
    }
}
