<?php

namespace App\Database\Seed;

use PetStore\Framework\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{
    // Include all seeds so they can be added to the collection.
    public function __construct(
        Attribute $attribute,
        Category $category,
        Product $product,
        ProductAttribute $productAttribute,
        ProductCategory $productCategory
    ) {
        // These are added in a prioritized order for FK relationships.
        $this->setItems([
            $product,
            $category,
            $attribute,
            $productCategory,
            $productAttribute,
        ]);
    }
}
