<?php

namespace App\Database\Seed;

use PetStore\Framework\Database\MaintenanceInterface;
use App\Database\Model\ProductCategory as Model;
use App\Database\Model\Product;
use App\Database\Model\Category;

final class ProductCategory implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        $products = Product::all();
        $cats = Category::all();

        foreach ($products as $product) {
            foreach ($cats as $cat) {
                $match = \strstr($product->sku, $cat->slug) !== false;

                if ($match) {
                    Model::insert([
                        'product_id' => $product->id,
                        'category_id' => $cat->id
                    ]);

                    break;
                }
            }
        }
    }

    public function rollback()
    {
        Model::truncate();
    }
}
