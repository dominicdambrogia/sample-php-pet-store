<?php

namespace App\Database\Seed;

use PetStore\Framework\Database\MaintenanceInterface;
use App\Database\Model\ProductAttribute as Model;
use App\Database\Model\Attribute;
use App\Database\Model\Product;

final class ProductAttribute implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        $attributeMap = self::getMappedAttributeValues();

        foreach ($attributeMap as $sku => $attributes) {
            foreach ($attributes as $attr => $value) {
                self::setAttributeToSku($sku, $attr, $value);
            }
        }
    }

    public function rollback()
    {
        Model::truncate();
    }

    /**
     * A helper function to set attributes to a product.
     * @param string $sku - the product sku
     * @param string $name - the name of the attribute
     * @param string|array $value - the value of the attribute
     * @return void
     */
    private static function setAttributeToSku($sku, $name, $value)
    {
        // A few products have multiple values for the same attr,
        // recurses through these to set them all.
        if (is_array($value)) {
            foreach ($value as $val) {
                self::setAttributeToSku($sku, $name, $val);
            }
            return;
        }

        Model::create([
            'product_id' => Product::where(['sku' => $sku])->first()->id,
            'attribute_id' => Attribute::where(['name' => $name])->first()->id,
            'value' => $value
        ]);
    }

    /**
     * A helper function to layout the products and what attributes they're
     * supposed to have.
     * @return void
     */
    private static function getMappedAttributeValues()
    {
        return [
            'animal-dog-dachshund-small' => [
                'age' => 5,
                'life-span' => 9,
                'name' => 'Adopt a Dog - Tyrese',
                'pet-type' => 'dog',
                'price' => 50.00,
                'pet-type' => 'dog',
                'size' => 'small'
            ],
            'animal-dog-chihuahua-small' => [
                'age' => 3,
                'life-span' => 9,
                'name' => 'Adopt a Dog - Wanda',
                'pet-type' => 'dog',
                'price' => 60.00,
                'size' => 'small'
            ],
            'animal-dog-husky-large' => [
                'age' => 4,
                'life-span' => 12,
                'name' => 'Adopt a Dog - Koba',
                'pet-type' => 'dog',
                'price' => 80.00,
                'size' => 'large'
            ],
            'animal-dog-lab-large' => [
                'age' => 7,
                'life-span' => 8,
                'name' => 'Adopt a Dog - Ol\' Jimmy',
                'pet-type' => 'dog',
                'price' => 25.00,
                'size' => 'large'
            ],
            'animal-dog-pit-large' => [
                'age' => 3,
                'life-span' => 9,
                'name' => 'Adopt a Dog - Mick',
                'pet-type' => 'dog',
                'price' => 50.00,
                'size' => 'large'
            ],

            'animal-cat-kitten-small' => [
                'age' => 0,
                'life-span' => 12,
                'name' => 'Adopt a Cat - Rex',
                'pet-type' => 'cat',
                'price' => 30.00,
                'size' => 'small'
            ],
            'animal-cat-black-small' => [
                'age' => 2,
                'life-span' => 12,
                'name' => 'Adopt a Cat - Shadow',
                'pet-type' => 'cat',
                'price' => 20.00,
                'size' => 'small'
            ],
            'animal-cat-grey-large' => [
                'age' => 7,
                'life-span' => 12,
                'name' => 'Adopt a Cat - Carl',
                'pet-type' => 'cat',
                'price' => 35.00,
                'size' => 'large'
            ],
            'animal-cat-white-large' => [
                'age' => 6,
                'life-span' => 12,
                'name' => 'Adopt a Cat - Ghost',
                'pet-type' => 'cat',
                'price' => 50.00,
                'size' => 'large'
            ],
            'animal-cat-orange-large' => [
                'age' => 8,
                'life-span' => 12,
                'name' => 'Adopt a Cat - Tigger',
                'pet-type' => 'cat',
                'price' => 45.00,
                'size' => 'large'
            ],

            'animal-reptile-gecko-small' => [
                'age' => 1,
                'life-span' => 6,
                'name' => 'Adopt a Gecko - Gary',
                'pet-type' => 'reptile',
                'price' => 15.00,
                'size' => 'small'
            ],
            'animal-reptile-bearded-dragon-small' => [
                'age' => 1,
                'life-span' => 6,
                'name' => 'Adopt a Dragon - Daenerys',
                'pet-type' => 'reptile',
                'price' => 35.00,
                'size' => 'small'
            ],
            'animal-reptile-iguana-large' => [
                'age' => 3,
                'life-span' => 6,
                'name' => 'Adopt an Iguana - Andre',
                'pet-type' => 'reptile',
                'price' => 45.00,
                'size' => 'large'
            ],
            'animal-reptile-boa-large' => [
                'age' => 7,
                'life-span' => 12,
                'name' => 'Adopt a Snake - Sssir Sean',
                'pet-type' => 'reptile',
                'price' => 70.00,
                'size' => 'large'
            ],
            'animal-reptile-python-large' => [
                'age' => 8,
                'life-span' => 12,
                'name' => 'Adopt a Sname - Peter',
                'pet-type' => 'reptile',
                'price' => 75.00,
                'size' => 'large'
            ],

            'accessory-crate-small' => [
                'name' => 'Small Crate',
                'pet-type' => ['cat', 'dog'],
                'price' => 45.00,
                'size' => 'small'
            ],
            'accessory-crate-large' => [
                'name' => 'Large Crate',
                'pet-type' => ['cat', 'dog'],
                'price' => 60.00,
                'size' => 'large'
            ],
            'accessory-terrarium-small' => [
                'name' => 'Small Terrarium',
                'pet-type' => 'reptile',
                'price' => 45.00,
                'size' => 'small'
            ],
            'accessory-terrarium-large' => [
                'name' => 'Large Terrarium',
                'pet-type' => 'reptile',
                'price' => 95.00,
                'size' => 'large'
            ],
            'accessory-cat-house-large' => [
                'name' => 'Large Cat House',
                'pet-type' => 'cat',
                'price' => 70.00,
                'size' => 'large'
            ],

            'toy-dog-ball' => [
                'name' => 'Tennis Balls',
                'pet-type' => 'dog',
                'price' => 10.00,
            ],
            'toy-cat-nip' => [
                'name' => 'Cat Nip',
                'pet-type' => 'cat',
                'price' => 8.25,
            ],
            'toy-cat-mouse' => [
                'name' => 'Cat n\' Mouse',
                'pet-type' => 'cat',
                'price' => 5.00,
            ],
            'toy-terrarium-rock' => [
                'name' => 'Terrarium Rock',
                'pet-type' => 'reptile',
                'price' => 20.00,
            ],
            'toy-terrarium-bowl' => [
                'name' => 'Terrarium Bowl',
                'pet-type' => 'reptile',
                'price' => 25.00,
            ],
        ];
    }
}
