<?php

namespace App\Database\Seed;

use PetStore\Framework\Database\MaintenanceInterface;
use App\Database\Model\Attribute as Model;

final class Attribute implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        $atts = [
            ['age', false, true],
            ['life-span', false, true],
            ['name', true, false],
            ['pet-type', false, true],
            ['price', true, false],
            ['size', false, true],
        ];

        $atts = array_map(function ($attr) {
            return [
                'name' => $attr[0],
                'can_sort' => $attr[1],
                'can_filter' => $attr[2]
            ];
        }, $atts);

        Model::insert($atts);
    }

    public function rollback()
    {
        Model::truncate();
    }
}
