<?php

namespace App\Database\Seed;

use PetStore\Framework\Database\MaintenanceInterface;
use App\Database\Model\Product as Model;

final class Product implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Model::insert([
            ['sku' => 'animal-dog-dachshund-small'],
            ['sku' => 'animal-dog-chihuahua-small'],
            ['sku' => 'animal-dog-husky-large'],
            ['sku' => 'animal-dog-lab-large'],
            ['sku' => 'animal-dog-pit-large'],

            ['sku' => 'animal-cat-kitten-small'],
            ['sku' => 'animal-cat-black-small'],
            ['sku' => 'animal-cat-grey-large'],
            ['sku' => 'animal-cat-white-large'],
            ['sku' => 'animal-cat-orange-large'],

            ['sku' => 'animal-reptile-gecko-small'],
            ['sku' => 'animal-reptile-bearded-dragon-small'],
            ['sku' => 'animal-reptile-iguana-large'],
            ['sku' => 'animal-reptile-boa-large'],
            ['sku' => 'animal-reptile-python-large'],

            ['sku' => 'accessory-crate-small'],
            ['sku' => 'accessory-crate-large'],
            ['sku' => 'accessory-terrarium-small'],
            ['sku' => 'accessory-terrarium-large'],
            ['sku' => 'accessory-cat-house-large'],

            ['sku' => 'toy-dog-ball'],
            ['sku' => 'toy-cat-nip'],
            ['sku' => 'toy-cat-mouse'],
            ['sku' => 'toy-terrarium-rock'],
            ['sku' => 'toy-terrarium-bowl'],
        ]);
    }

    public function rollback()
    {
        Model::truncate();
    }
}
