<?php

namespace App\Database\Seed;

use PetStore\Framework\Database\MaintenanceInterface;
use App\Database\Model\Category as Model;

final class Category implements MaintenanceInterface
{
    public function table()
    {
        return (new Model)->getTable();
    }

    public function commit()
    {
        Model::insert([
            [
                'name' => 'Animal',
                'slug' => 'animal',
            ], [
                'name' => 'Toy',
                'slug' => 'toy',
            ], [
                'name' => 'Accessory',
                'slug' => 'accessory'
            ]
        ]);
    }

    public function rollback()
    {
        Model::truncate();
    }
}
