<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

final class Product extends Model
{
    protected $guarded = [];

    /**
     * Get the product_categories related to this product.
     */
    public function productCategories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    /**
     * Get the product_attributes related to this product.
     */
    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * Get the categories this product is related to through the
     * product_categories table.
     */
    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            (new ProductCategory)->getTable()
        );
    }

    /**
     * Get the attribtues this product is related to through the
     * product_attributes table.
     */
    public function attributes()
    {
        return $this->belongsToMany(
            Attribute::class,
            (new ProductAttribute)->getTable()
        );
    }
}
