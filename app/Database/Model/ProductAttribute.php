<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

final class ProductAttribute extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    /**
     * Get the product this attribute_value belongs to.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the attribute this attribute_value belongs to.
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
