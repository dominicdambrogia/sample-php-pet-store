<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

final class ProductCategory extends Model
{
    protected $guarded = [];

    /**
     * Get the product this product_category belongs to.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the category this product_category belongs to.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
