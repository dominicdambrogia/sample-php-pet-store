<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

final class Attribute extends Model
{
    protected $guarded = [];

    /**
     * Return the products that this attribute is assigned to through the
     * product_attribtues table.
     */
    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            (new ProductAttribute)->getTable()
        );
    }

    /**
     * Return the product_attributes that this attribute is related to.
     */
    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * Get the distinct values available for this attribute.
     */
    public function distinctValues()
    {
        return $this->productAttributes()
            ->getQuery()
            ->join('attributes', 'attributes.id', '=', 'product_attributes.attribute_id')
            ->groupBy('product_attributes.value')
            ->get(['product_attributes.value'])
            ->pluck('value');
    }
}
