<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

final class Category extends Model
{
    protected $guarded = [];

    /**
     * Get the product_categories that belong to this category.
     */
    public function products()
    {
        return $this->hasMany(ProductCategory::class);
    }
}
