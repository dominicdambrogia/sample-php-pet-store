<?php

namespace App\Console;

use PetStore\Framework\Support\Collection as BaseCollection;

final class Collection extends BaseCollection
{
    /**
     * A user-defined array of the commands.
     * All the commands will be registered within the application.
     */
    protected $items = [
        Command\Catalog\Attributes::class,
        Command\Catalog\Categories::class,
        Command\Catalog\Products::class,
        Command\Database\Migrate::class,
        Command\Database\Seed::class,
    ];
}
