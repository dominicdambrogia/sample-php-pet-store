<?php

namespace App\Console\Command\Catalog;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use App\Database\Model\Attribute;
use Symfony\Component\Console\Helper\TableSeparator;

class Attributes extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('catalog:attributes');
        $this->setDescription('Display information about the available attribtues');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->showAvailable($output);
    }

    /**
     * Show all available attributes and their values (up to x chars).
     * @return void
     */
    private function showAvailable(OutputInterface $output)
    {
        $table = (new Table($output))
            ->setHeaders(['id', 'Attribute', 'Can Sort', 'Can Filter', 'Values'])
            ->setColumnWidths([5, 10, 10, 10, 45]);

        $attributes = Attribute::all();
        foreach ($attributes as $i => $attr) {
            $table->addRow([$attr->id, $attr->name, $attr->can_sort, $attr->can_filter, '']);

            foreach ($attr->distinctValues() as $value) {
                $table->addRow(['', '', '', '', $value]);
            }

            if ($attributes->count() > $i + 1) {
                $table->addRow(new TableSeparator());
            }
        }

        $table->render();
    }
}
