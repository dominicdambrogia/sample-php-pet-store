<?php

namespace App\Console\Command\Catalog;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use App\Database\Model\Category;
use App\Service\Catalog\Category\CategoryList;

class Categories extends Command
{
    public function __construct(CategoryList $categoryList)
    {
        $this->categoryList = $categoryList;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('catalog:categories');
        $this->setDescription('Display information about the available categories');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->showAvailable($output);
    }

    /**
     * List the available categories.
     * @return void
     */
    private function showAvailable($output)
    {
        (new Table($output))
            ->setHeaders(['id', 'name'])
            ->setColumnWidths([3, 15])
            ->setRows($this->categoryList->prepare())
            ->render();
    }
}
