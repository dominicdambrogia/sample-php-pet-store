<?php

namespace App\Console\Command\Catalog;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use App\Database\Model\Category;
use App\Service\Catalog\Product\ProductList;
use Symfony\Component\Console\Helper\TableSeparator;

class Products extends Command
{
    protected $productList;

    public function __construct(ProductList $productList)
    {
        $this->productList = $productList;
        parent::__construct();
    }

    protected function configure()
    {
        // phpcs:disable Generic.Files.LineLength
        $this->setName('catalog:products');
        $this->setDescription('Search products in the catalog. Utilize attributes and categories to sort and filter');
        $this->addOption('category', null, InputOption::VALUE_REQUIRED, 'Filter products to a specific category', '');
        $this->addOption('desc', null, InputOption::VALUE_NONE, 'Sort Products in descending order - useless without sort option');
        $this->addOption('filter', null, InputOption::VALUE_REQUIRED, 'Filter Products by filterable attribute with query string syntax: attr=val', '');
        $this->addOption('sort', null, InputOption::VALUE_REQUIRED, 'Sort Products by sortable attribute', '');
        // phpcs:enable
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $category = $input->getOption('category');
            $desc = $input->getOption('desc');
            parse_str($input->getOption('filter'), $filter);
            $sort = $input->getOption('sort');

            $this->assertInputArguments($category, $filter, $sort);

            $this->productList = $this->productList->prepare(
                $this->getCategoryId($category, $output),
                $filter,
                ['attribute' => $sort, 'direction' => $desc ? 'desc' : 'asc']
            );

            $this->show($output);
        } catch (\Throwable $th) {
            if ($output->isDebug()) {
                throw $th;
            } else {
                $output->writeln('<error>' . $th->getMessage() . '</error>');
            }
        }
    }

    /**
     * Get category id of the passed category, return 0 on no cat.
     * @return int
     */
    public function getCategoryId($category, $output = null)
    {
        $cat = empty($category) ? 0 : Category::where(['slug' => $category])->first();
        if ($cat === null) {
            throw new \Exception("Invalid category: $category");
        } else {
            return $cat->id;
        }
    }

    /**
     * Assert the CLI input arguments are in the expected format/type/size.
     * @return void
     */
    private function assertInputArguments($cat, $filter, $sort)
    {
        $regex = '/([a-zA-Z|-])+/';
        if (! empty($cat) && ! preg_match($regex, $cat)) {
            throw new \Exception("Invalid category: $cat");
        }

        if (! empty($sort) && ! preg_match($regex, $sort)) {
            throw new \Exception("Invalid sort: $sort");
        }

        foreach ($filter as $key => $value) {
            if (! preg_match($regex, $key)) {
                throw new \Exception("Invalid filter key: $key");
            }

            if (! preg_match($regex, $value)) {
                throw new \Exception("Invalid filter value: $key");
            }
        }

        if (count($filter) > 1) {
            throw new \Exception("Only 1 filter allowed.");
        }
    }

    /**
     * Show the results in a formatted table.
     */
    private function show($output)
    {
        $table = new Table($output);
        $table->setHeaders(['id', 'sku', 'attribute', 'values']);

        foreach ($this->productList as $i => $product) {
            $table->addRow([$product->getId(), $product->getSku(), '', '']);

            foreach ($product->getAttributeKeys() as $attr) {
                $table->addRow(['', '', $attr, '']);

                foreach ($product->getAttributeValues($attr) as $value) {
                    $table->addRow(['', '', '', (string) $value]);
                }
            }

            if ($this->productList->count() > $i + 1) {
                $table->addRow(new TableSeparator());
            }
        }

        $table->render();
    }
}
