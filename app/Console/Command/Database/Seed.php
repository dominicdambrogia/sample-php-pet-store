<?php

namespace App\Console\Command\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\Seed\Collection;

class Seed extends Command
{
    /**
     * @see developer note:
     * Similar to the note in Migrate, I wanted to have the autonomy of seeding
     * the database but it wasn't really in scope so the seeding scripts are
     * pretty minimal.
     */

    public function __construct(Collection $seedCollection)
    {
        $this->seedCollection = $seedCollection;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('database:seed');
        $this->setDescription('Load sample data into the application');

        $msg = 'Remove all sample data by emptying all the tables';
        $this->addOption('rollback', null, null, $msg, null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Is this a commit or rollback action?
        $rollback = $input->getOption('rollback');
        $method = ! $rollback ? 'commit' : 'rollback';
        // Run migrations in reverse for rollbacks becauase of FK
        $seeds = ! $rollback
            ? $this->seedCollection->getItems()
            : $this->seedCollection->reverse();

        foreach ($seeds as $seed) {
            try {
                $seed->$method();
                $output->writeln("seed {$method}: {$seed->table()}");
            } catch (\Throwable $th) {
                if ($output->isDebug()) {
                    throw $th;
                } else {
                    $output->writeln("{$seed->table()}: {$th->getMessage()}");
                }
            }
        }
    }
}
