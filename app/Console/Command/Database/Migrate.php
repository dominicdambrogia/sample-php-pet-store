<?php

namespace App\Console\Command\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Database\Migration\Collection as MigrationCollection;

class Migrate extends Command
{
    /**
     * @see developer note:
     * I wanted to have the autonomy of running migration scripts, but on the
     * other hand it wasn't really in the scope of the example to build out a
     * fully featured migration feature. Because of that I knowingly kept these
     * migration scripts pretty binary - either bring them up or tear them down.
     */

    public function __construct(MigrationCollection $migrationCollection)
    {
        $this->migrationCollection = $migrationCollection;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('database:migrate');
        $this->setDescription('Setup default schema by running all migrations');

        $msg = 'Rollback changes by running rollback command on all migrations';
        $this->addOption('rollback', null, null, $msg, null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Is this a commit or rollback action?
        $rollback = $input->getOption('rollback');
        $method = ! $rollback ? 'commit' : 'rollback';
        // Run migrations in reverse for rollbacks becauase of FK
        $migrations = ! $rollback
            ? $this->migrationCollection->getItems()
            : $this->migrationCollection->reverse();

        foreach ($migrations as $migration) {
            try {
                $migration->$method();
                $output->writeln("migrate {$method}: {$migration->table()}");
            } catch (\Throwable $th) {
                if ($output->isDebug()) {
                    throw $th;
                } else {
                    $output->writeln("{$migration->table()}: {$th->getMessage()}");
                }
            }
        }
    }
}
