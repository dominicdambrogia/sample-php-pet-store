<?php

namespace App\Service\Catalog\Product;

use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use App\Database\Model\Attribute;
use App\Database\Model\Category;
use App\Database\Model\Product;
use League\Event\Emitter;
use PetStore\Framework\Cache\Cacheable;
use Psr\SimpleCache\CacheInterface;

class ProductList
{
    use Cacheable;

    protected $productFactory;
    protected $emitter;
    protected $cache;

    public function __construct(Factory $productFactory, Emitter $emitter, CacheInterface $cache)
    {
        $this->productFactory = $productFactory;
        $this->emitter = $emitter;
        $this->cache = $cache;
    }

    /**
     * Prepare a collection of loaded products based on the passed in parameters.
     * @return Collection
     */
    public function prepare(int $categoryId, array $filter, array $sort): Collection
    {
        $cacheKey = $this->getCacheKey([$categoryId, $filter, $sort]);

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        // Query products if no cat, else products + cat.
        $query = $categoryId === 0
            ? (new Product)->select(['products.id'])
            : Category::find($categoryId)->products()->getQuery()
                ->join('products', 'products.id', '=', 'product_categories.product_id');
        // Filter products based on filter
        $query = ! empty($filter) ? $this->addFilters($query, $filter) : $query;
        // Sort products by sort attribtue
        $query = ! empty($sort['attribute']) ? $this->addSort($query, $sort) : $query;
        // Load sorted product ids,
        $ids = $query->get(['products.id'])->pluck('id');
        // Load products - can invalidate sort method (ex: pricing & sales)
        $products = $ids->map(function ($productId) {
            return $this->productFactory->create($productId);
        });
        // Allow for hook to resort products after products load.
        $products = $this->sortAfterLoad($products, $sort);
        // Update cache and return value
        $this->cache->set($cacheKey, $products);
        return $products;
    }

    /**
     * @see Cacheable
     */
    public function cachePrefix(): string
    {
        return 'product_list_';
    }

    /**
     * Apply the [$attr => $value] filter set to filter the products
     * within the $query.
     * @param QueryBuilder $query
     * @param array $filters
     * @return QueryBuilder
     */
    private function addFilters(QueryBuilder $query, array $filters): QueryBuilder
    {
        $attributeIds = [];
        $attributeValues = [];

        foreach ($filters as $name => $value) {
            if (! $attr = Attribute::where(['name' => $name])->first()) {
                throw new \Exception("Invalid filter: $name");
            }

            if (! $attr->can_filter) {
                throw new \Exception("Attribute is not filterable: $name");
            }

            if (! in_array($value, $attr->distinctValues()->toArray())) {
                throw new \Exception("Invalid filter value: $name => $value");
            }

            $attributeIds[] = $attr->id;
            $attributeValues[] = $value;
        }

        $query->join('product_attributes', 'product_attributes.product_id', '=', 'products.id');
        $query->join('attributes', 'attributes.id', '=', 'product_attributes.attribute_id');
        $query->whereIn('attributes.id', $attributeIds);
        $query->whereIn('product_attributes.value', $attributeValues);

        return $query;
    }

    /**
     * Use the $sort parameter to order the query.
     * @param QueryBuilder $query
     * @param array $sort
     * @return QueryBuilder
     */
    private function addSort(QueryBuilder $query, array $sort): QueryBuilder
    {
        $attrName = $sort['attribute'];
        $sortDirection = $sort['direction'];

        if (! $attr = Attribute::where(['name' => $attrName])->first()) {
            throw new \Exception("Invalid sort: $attrName");
        }

        $ids = $query->get(['products.id'])->pluck('id')->toArray();

        return Product::whereIn('products.id', $ids)
            ->join('product_attributes', 'product_attributes.product_id', '=', 'products.id')
            ->where('attribute_id', '=', $attr->id)
            ->orderBy('value', $sortDirection);
    }

    /**
     * Dispatch an event to allow for other services to mutate the collection
     * after it is collected. Helpful for sales invalidating sales order, etc.
     * @param Collection $loadedProducts
     * @param array $sort
     * @return Collection
     */
    private function sortAfterload(Collection $loadedProducts, array $sort): Collection
    {
        $event = $this->emitter->emit('product_list_sort_after_load', [
            'products' => $loadedProducts,
            'sort' => $sort
        ]);

        return isset($event->data) ? $event->data : $loadedProducts;
    }
}
