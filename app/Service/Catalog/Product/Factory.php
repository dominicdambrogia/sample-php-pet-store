<?php

namespace App\Service\Catalog\Product;

use App\Database\Model\Product;
use App\Database\Model\Attribute;
use League\Event\Emitter;
use PetStore\Framework\Cache\Cacheable;
use Psr\SimpleCache\CacheInterface;

class Factory
{
    use Cacheable;

    protected $attributes;
    protected $product;
    protected $emitter;
    protected $cache;

    public function __construct(Emitter $emitter, CacheInterface $cache)
    {
        $this->emitter = $emitter;
        $this->cache = $cache;
    }

    /**
     * @see Cacheable
     */
    public function cachePrefix(): string
    {
        return 'product_factory_';
    }

    /**
     * Create a product, set it with all of it's attributes and
     * emit an event that a product is being loaded so that others can
     * tie in to this functionality. Attempt to load from cache
     * @param int $id
     * @return self
     */
    public function create(int $id): self
    {
        $cacheKey = $this->getCacheKey($id);

        if (! $this->cache->has($cacheKey)) {
            $this->product = Product::find($id);
            $this->setAttributes($this->product);
            $product = clone $this->emitEvent();
            $this->cache->set($cacheKey, $product, 3600);
        } else {
            $product = $this->cache->get($cacheKey);
        }

        return $product;
    }

    /**
     * Dispatch an event to allow others to hook in and mutate the data
     * that is loaded into this product.
     * @return self
     */
    protected function emitEvent(): self
    {
        $event = $this->emitter->emit('product_factory_create', $this);

        return isset($event->data) ? $event->data : $this;
    }

    /**
     * Get the sku of the product.
     * @return string
     */
    public function getSku()
    {
        return $this->product->sku;
    }

    /**
     * Get the id of the product.
     * @return int
     */
    public function getId()
    {
        return $this->product->id;
    }

    /**
     * Get available attribute keys
     * @return array
     */
    public function getAttributeKeys()
    {
        return array_keys($this->attributes);
    }

    /**
     * Check if this product has an attribute
     * @param string $attrName
     * @param bool $throw
     * @return bool
     */
    public function hasAttribute($attrName, $throw = false)
    {
        $exists = isset($this->attributes[$attrName]);

        if (! $exists && $throw) {
            throw new \Exception("Invalid product attribute: $attrName");
        }

        return $exists;
    }

    /**
     * Attributes are stored in an array by default, because each product
     * can hold more than one value for a product. This will fetch that
     * array of values.
     * @param string $attrName
     * @return array
     */
    public function getAttributeValues($attrName)
    {
        $this->hasAttribute($attrName, true);

        return $this->attributes[$attrName];
    }

    /**
     * Get single attribute value. Use this if you expect a single value within
     * the array of values for this product attribute.
     * @param string $attrName
     * @return array
     */
    public function getAttributeValue($attrName)
    {
        return $this->getAttributeValues($attrName)[0];
    }

    /**
     * Add another value on to the end of the product_attributes values.
     * @param string $attrName
     * @param mixed $value
     * @return void
     */
    public function appendAttribute($attrName, $value)
    {
        $this->attributes[$attrName][] = $value;
    }

    /**
     * Create/Overwrite an entire key within the attrinute set.
     * @param string $attrName
     * @param mixed $value
     * @return void
     */
    public function setAttribute($attrName, $value)
    {
        $this->attributes[$attrName] = is_array($value) ? $value : [$value];
    }

    /**
     * Set the attributes and their values on to the product.
     * @param Product $product
     * @return void
     */
    protected function setAttributes(Product $product)
    {
        $this->attributes = [];

        foreach ($product->productAttributes as $productAttr) {
            $attr = Attribute::find($productAttr->attribute_id);
            $this->attributes[$attr->name][] = $productAttr->value;
        }
    }

    /**
     * Get all of the attributes assigned to this product.
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
