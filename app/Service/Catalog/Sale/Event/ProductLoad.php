<?php

namespace App\Service\Catalog\Sale\Event;

use PetStore\Framework\Event\AbstractListener;
use League\Event\EventInterface;

class ProductLoad extends AbstractListener
{
    public function targetEvent(): string
    {
        return 'product_factory_create';
    }

    public function execute(EventInterface $event, $product = null): EventInterface
    {
        // Configure age discount of item.
        if ($product->hasAttribute('age') &&
            $product->hasAttribute('life-span') &&
            $product->getAttributeValue('age') * 2 >= $product->getAttributeValue('life-span')
        ) {
            $newPrice = $product->getAttributeValue('price') / 2;
            $product->setAttribute('price', $newPrice);
            $product->setAttribute('on-sale', "true");
        } else {
            $product->setAttribute('on-sale', "false");
        }

        $event->data = $product;

        return $event;
    }
}
