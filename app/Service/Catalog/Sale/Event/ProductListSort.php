<?php

namespace App\Service\Catalog\Sale\Event;

use PetStore\Framework\Event\AbstractListener;
use League\Event\EventInterface;

class ProductListSort extends AbstractListener
{
    public function targetEvent(): string
    {
        return 'product_list_sort_after_load';
    }

    public function execute(EventInterface $event, $eventData = null): EventInterface
    {
        if ($eventData['sort']['attribute'] == 'price') {
            /**
             * With a larger data set it would be smart to optimize the
             * sorting method (merge sort, quick sort, etc.) but with < 100
             * products this isn't quite necessary yet.
             */

            // Function is used in either method below, broke up for readability.
            $sort = function ($product) {
                return $product->getAttributeValue('price');
            };

            $event->data = $eventData['sort']['direction'] == 'asc'
                ? $eventData['products']->sortBy($sort)
                : $eventData['products']->sortByDesc($sort);
        }

        return $event;
    }
}
