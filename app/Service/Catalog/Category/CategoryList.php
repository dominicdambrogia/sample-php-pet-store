<?php

namespace App\Service\Catalog\Category;

use App\Database\Model\Category;

class CategoryList
{
    /**
     * Prepare the categories to be output into the console.
     * @return array
     */
    public function prepare(): array
    {
        return Category::all()->map(function ($cat) {
            return [$cat->id, $cat->slug];
        })->toArray();
    }
}
