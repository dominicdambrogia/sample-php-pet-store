<?php

namespace App\Event;

use PetStore\Framework\Support\Collection as BaseCollection;

final class Collection extends BaseCollection
{
    /**
     * A user-defined array of the event listeners in our collection.
     * All of the listeners will be registered within the application.
     */
    protected $items = [
        \App\Service\Catalog\Sale\Event\ProductListSort::class,
        \App\Service\Catalog\Sale\Event\ProductLoad::class,
    ];
}
